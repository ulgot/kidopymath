import random
import sys

PREFIX = r"""
\documentclass[12pt, a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{multicol}
\usepackage[cm]{fullpage}
\begin{document}
\begin{multicols}{2}
{\large
"""
SUFFIX = r"""
}
\end{multicols}
\end{document}"""


def get_examples(N=50, maks=20):
    """a b / c +-*/ D e / f = """
    a = [random.randint(1, 9) for i in range(N)]
    b = [random.randint(1, maks) for i in range(N)]
    c = [random.randint(2, maks) for i in range(N)]
    d = [random.randint(1, 9) for i in range(N)]
    e = [random.randint(1, maks) for i in range(N)]
    f = [random.randint(2, maks) for i in range(N)]
    znak = []
    i = 0
    while i < N:
        A = a[i] + b[i] / float(c[i])
        B = d[i] + e[i] / float(f[i])
        r = random.random()
        if r < 0.25:
            znak.append('+')
        elif 0.25 <= r < 0.5: 
            znak.append('\cdot')
        elif 0.5 <= r < 0.75:
            znak.append(':')
        else:
            if A < B:
                a[i], d[i] = d[i], a[i]
                b[i], e[i] = e[i], b[i]
                c[i], f[i] = f[i], c[i]
            znak.append('-')
        if a[i] == 1:
            a[i] = ""
        if d[i] == 1:
            d[i] = ""
        i += 1
    return zip(a, b, c, znak, d, e, f)


def latexify(examples):
  ret = PREFIX
  ret += r"""\begin{enumerate}
  """
  for ex in examples:
    ret += r"""\item {} $\frac{{{}}}{{{}}} {} {} \frac{{{}}}{{{}}}=$
    """.format(*ex)
  ret += r"""\end{enumerate}
  """
  ret += SUFFIX
  return ret


def parse_args(args):
  print args
  ret = {}
  ret['N'] = 50
  ret['maks'] = 20
  if len(args) == 2:
    ret['N'] = int(args[1])
  elif len(args) == 3:
    ret['N'] = int(args[1])
    ret['maks'] = int(args[2])
  return ret


if __name__ == "__main__":
  d = parse_args(sys.argv)
  N = d['N']
  maks = d['maks']
  with open("ulamki_example.tex", 'w') as f:
    f.write(latexify(get_examples(N, maks)))

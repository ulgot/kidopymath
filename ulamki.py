from random import randint


def doc_begin_end(columns=2):
    col = {1: 'onecolumn', 2: 'twocolumns'}

    BEGINDOC = r"""\documentclass[a4paper, 12pt, %s]{article}
    \begin{document}
    \begin{enumerate}""" % col[columns]

    ENDDOC = r"""\end{enumerate}
    \end{document}"""

    return BEGINDOC, ENDDOC


def list_element(u):
    """dodaje ulamek do listy"""
    ret = r"\item ${} \frac{{{}}}{{{}}}$".format(*u)
    return ret


def ulamek(maksimum):
    C = randint(2, maksimum)
    B = randint(1, C - 1)
    A = randint(1, maksimum)
    return (A, B, C)


def generuj_ulamki(N, maksimum):
    return [ulamek(maksimum) for i in xrange(N)]


def ulamki_latex(N, maksimum):
    begin, end = doc_begin_end()
    ret = begin
    ulamki_list = generuj_ulamki(N, maksimum)
    for u in ulamki_list:
        ret += list_element(u)
    ret += end
    return ret


def save_latex_file(txt, name='ulamki.tex'):
    with open(name, 'w') as f:
        f.write(txt)
        
        
if __name__ == "__main__":
    save_latex_file(ulamki_latex(40, 20))